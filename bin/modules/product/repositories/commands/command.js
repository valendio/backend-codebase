class Command {
  constructor(db) {
    this.db = db;
  }

  async insertProduct(product) {
    const { name, categories, price, details, isActive } = product;
    const query = `INSERT INTO products (name, categories, price, details, isActive) VALUES ('${name}','${categories}',${price},'${details}','${isActive}')`;
    const result = await this.connection.query(query);
    return result;
  }

  async updateOneProduct(product) {
    const { id, name, categories, price, details, isActive } = product;
    const query = `UPDATE products SET name = '${name}', categories = '${categories}', price = ${price}, details = '${details}', isActive = '${isActive}' WHERE id = ${id}`;
    const result = await this.connection.query(query);
    return result;
  }

  async deleteOneProduct(id) {
    const query = `DELETE FROM products WHERE id = ${id}`;
    const result = await this.connection.query(query);
    return result;
  }
  
}

module.exports = Command;
