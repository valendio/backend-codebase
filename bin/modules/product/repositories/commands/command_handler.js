const Product = require("./domain");
const Mongo = require("../../../../helpers/databases/mongodb/db");
const Mysql = require("../../../../helpers/databases/mysql/db");
const config = require("../../../../infra/configs/global_config");
const db = new Mysql(config.get("/mysqlConfig"));

const insertProduct = async (payload) => {
  const product = new Product(db);
  const postCommand = async (payload) => product.create(payload);
  return postCommand(payload);
};

const deleteProduct = async (payload) => {
  const product = new Product(db);
  const postCommand = async (payload) => product.delete(payload);
  return postCommand(payload);
};

module.exports = {
  insertProduct,
  deleteProduct,
};
