const Query = require("../queries/query");
const Command = require("./command");
const mysql = require("mysql");
const wrapper = require("../../../../helpers/utils/wrapper");
const logger = require("../../../../helpers/utils/logger");

class Product {
  constructor(db) {
    this.db = db;

    // Create a connection pool
    this.pool = mysql.createPool(db);
  }

  async create(payload) {
    const ctx = "domain-postProduct";

    try {
      // Start a transaction
      const connection = await this.getConnection();
      await this.beginTransaction(connection);

      // Insert the new product
      const result = await this.insertProduct(connection, payload);

      // Commit the transaction
      await this.commitTransaction(connection);

      return wrapper.data(result);
    } catch (err) {
      // Rollback the transaction on error
      const connection = await this.getConnection();
      await this.rollbackTransaction(connection);

      logger.log(ctx, "error", err.message);

      return wrapper.error(new InternalServerError("Failed to insert product"));
    } finally {
      // Release the connection back to the pool
      if (connection) {
        connection.release();
      }
    }
  }

  async delete(payload) {
    try {
      // Start a transaction
      const connection = await this.getConnection();
      await this.beginTransaction(connection);

      // Delete the product
      const result = await this.deleteOneProduct(connection, payload);

      // Commit the transaction
      await this.commitTransaction(connection);

      return wrapper.data(result);
    } catch (err) {
      // Rollback the transaction on error
      const connection = await this.getConnection();
      await this.rollbackTransaction(connection);

      logger.log(ctx, "error", err.message);

      return wrapper.error(new InternalServerError("Failed to delete product"));
    } finally {
      // Release the connection back to the pool
      if (connection) {
        connection.release();
      }
    }
  }

  async getConnection() {
    return new Promise((resolve, reject) => {
      this.pool.getConnection((err, connection) => {
        if (err) {
          reject(err);
        } else {
          resolve(connection);
        }
      });
    });
  }

  async beginTransaction(connection) {
    return new Promise((resolve, reject) => {
      connection.beginTransaction((err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  async commitTransaction(connection) {
    return new Promise((resolve, reject) => {
      connection.commit((err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  async rollbackTransaction(connection) {
    return new Promise((resolve, reject) => {
      connection.rollback(() => {
        resolve();
      });
    });
  }

  async insertProduct(connection, payload) {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO products SET ?", payload, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  async deleteOneProduct(connection, payload) {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM products WHERE id = ?",
        payload.id,
        (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        }
      );
    });
  }
}

module.exports = Product;
