class Query {
  constructor(connection) {
    this.connection = connection;
  }

  async findById(id) {
    const sql = `SELECT * FROM product WHERE id = ?`;
    const parameter = [id];
    const [rows, fields] = await this.connection.execute(sql, parameter);
    return rows[0];
  }

  async findAll() {
    const sql = `SELECT * FROM product`;
    const [rows, fields] = await this.connection.execute(sql);
    return rows;
  }
}

module.exports = Query;
