const Product = require("./domain");
const mysql = require("mysql2/promise");
const config = require("../../../../infra/configs/global_config");

const pool = mysql.createPool({
  host: config.get("/localhost"),
  user: config.get("/root"),
  password: config.get("/"),
  database: config.get("/agreeultur"),
});

const getAllProduct = async () => {
  const [rows] = await pool.query("SELECT * FROM products");
  return rows;
};

const getProductById = async (id) => {
  const [rows] = await pool.query(`SELECT * FROM products WHERE id=${id}`);
  return rows;
};

module.exports = {
  getAllProduct,
  getProductById,
};
